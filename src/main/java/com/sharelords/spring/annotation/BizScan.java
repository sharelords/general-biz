package com.sharelords.spring.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: 通用biz路径下的接口注册，springboot配置扫描
 * @Author: 千古龙少
 * @Time: 2019/11/23 18:00
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({BizScannerRegister.class})
public @interface BizScan {

    /**
     * 扫描路径
     */
    String[] value() default {};

    /**
     * 扫描路径
     */
    String[] basePackages() default {};

    /**
     * 是否保留java资源文件（默认不保留）
     */
    boolean remainJavaSrcFile() default false;

    /**
     * java资源文件保存路径，默认项目上一层目录下新建的bizTempJavaFile文件夹内
     */
    String srcFilePath() default "";
}
