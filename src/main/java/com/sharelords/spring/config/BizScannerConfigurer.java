package com.sharelords.spring.config;

import com.sharelords.biz.config.GeneralBizConfig;
import com.sharelords.biz.register.MemoryClassBeanRegister;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

import java.util.logging.Logger;

/**
 * @author 千古龙少
 * @Description: 自动扫描配置(必须在service包扫描前配置)
 * @date 2019年8月31日 下午2:44:33
 */
public class BizScannerConfigurer implements BeanDefinitionRegistryPostProcessor {

    private static final Logger logger = Logger.getLogger("ClassPathBizScanner");

    /**
     * biz文件扫描路径
     */
    private String basePackages;
    /**
     * 是否保留java资源文件（默认不保留）
     */
    private boolean remainJavaSrcFile;
    /**
     * java资源文件保存路径，默认项目上一层目录下新建的bizTempJavaFile文件夹内
     */
    private String srcFilePath;

    public void setBasePackages(String basePackages) {
        this.basePackages = basePackages;
    }

    public void setRemainJavaSrcFile(boolean remainJavaSrcFile) {
        this.remainJavaSrcFile = remainJavaSrcFile;
    }

    public void setSrcFilePath(String srcFilePath) {
        this.srcFilePath = srcFilePath;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
        if (org.apache.commons.lang.StringUtils.isBlank(basePackages)) {
            logger.warning("biz文件扫描路径为空，请检查配置信息");
            return;
        }

        // 加载配置参数
        GeneralBizConfig config = GeneralBizConfig.getInstance();
        config.basePackages = this.basePackages;
        config.remainJavaSrcFile = this.remainJavaSrcFile;
        config.srcFilePath = this.srcFilePath;

        // 开始扫描及动态生成class文件
        new ClassPathBizScanner(config).initMethod();

        // 向spring容器注册
        new MemoryClassBeanRegister(registry).regist();
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) {

    }

}
