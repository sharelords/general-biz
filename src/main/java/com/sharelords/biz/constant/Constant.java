package com.sharelords.biz.constant;

/**
 * @author 千古龙少
 * @Description: general-biz相关属性设置
 * @date 2020年6月19日 下午15:52:33
 */
public class Constant {

    private Constant() {
    }

    public static final String CLASS_PATH = "CLASS_PATH";

    public static final String STRING_FLAG = "string:///";
}
