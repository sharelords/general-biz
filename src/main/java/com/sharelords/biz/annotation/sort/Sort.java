package com.sharelords.biz.annotation.sort;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 排序注解，作用于方法上
 * @date 2019年11月2日 下午8:26:05
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface Sort {

    String value();
}
