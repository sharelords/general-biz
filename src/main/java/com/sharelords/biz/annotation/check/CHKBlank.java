package com.sharelords.biz.annotation.check;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 参数校验注解，检查是否为空（包括null和空值）
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface CHKBlank {
}