package com.sharelords.biz.annotation.param;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 参数名（条件属性名称）
 * @date 2019年10月6日 下午6:06:33
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface Prop {

    String value();
}
