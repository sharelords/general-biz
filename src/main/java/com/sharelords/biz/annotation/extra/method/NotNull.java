package com.sharelords.biz.annotation.extra.method;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 条件注解，非null(该注解存在时，参数值失效)
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface NotNull {

    String[] value() default {};
}