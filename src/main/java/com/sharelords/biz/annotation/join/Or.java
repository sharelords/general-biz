package com.sharelords.biz.annotation.join;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 条件连接注解，或
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface Or {
}