package com.sharelords.biz.annotation.skip;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 条件注解，参数为null，不进行条件组装
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface NullSkip {
}