package com.sharelords.biz.annotation.global;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 条件注解，当所有查询条件都跳过时，使用该注解，展示所有记录
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface QueryAllWhileParamAllSkip {

}