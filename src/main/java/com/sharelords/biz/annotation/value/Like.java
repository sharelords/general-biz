package com.sharelords.biz.annotation.value;

import java.lang.annotation.*;

/**
 * @author 千古龙少
 * @Description: 条件注解，模糊匹配--像
 * @date 2019年7月17日 下午4:03:42
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface Like {

    /**
     * 模糊查询左侧，可为""或%或多个_
     */
    String left() default "%";

    /**
     * 模糊查询右侧，可为""或%或多个_
     */
    String right() default "%";

}