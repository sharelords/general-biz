package com.sharelords.biz.annotation.value;

import java.lang.annotation.*;

/**
 * @Description: 条件注解，不等于
 * @Author: 千古龙少
 * @Time: 2019/11/30 17:08
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Inherited
public @interface NotEqual {
}