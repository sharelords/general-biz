package com.sharelords.biz.util;

import com.sharelords.biz.util.InvokeUtil.ChildForMap;
import com.sharelords.biz.util.InvokeUtil.SimpleEntry;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author 千古龙少
 * @Description:class工具
 * @date 2019年9月8日 下午11:20:24
 */
public class ClassUtil {

    /**
     * @param clazz
     * @return
     * @Description:获取方法列表
     * @author 千古龙少
     * @date 2019年9月8日 下午11:25:39
     */
    public static List<Method> getMethodList(Class<?> clazz) {
        if (clazz == null) {
            return Collections.emptyList();
        }

        Method[] methods = clazz.getDeclaredMethods();
        if (methods == null || methods.length == 0) {
            return Collections.emptyList();
        }

        return InvokeUtil.toList(methods);
    }

    /**
     * @param clazzList
     * @return
     * @Description:获取方法列表
     * @author 千古龙少
     * @date 2019年9月8日 下午11:39:22
     */
    public static Map<Class<?>, List<Method>> getMethodMap(List<Class<?>> clazzList) {
        if (clazzList == null || clazzList.isEmpty()) {
            return Collections.emptyMap();
        }

        return InvokeUtil.transList2Map(clazzList, new ChildForMap<Class<?>, List<Method>, Class<?>>() {
            @Override
            public SimpleEntry<Class<?>, List<Method>> group(Class<?> clazz) {
                return new SimpleEntry<Class<?>, List<Method>>(clazz, getMethodList(clazz));
            }
        });
    }

    /**
     * @param origClass
     * @param classes
     * @return
     * @Description:判断class是否在指定class列表中
     * @author 千古龙少
     * @date 2019年9月22日 下午6:08:01
     */
    public static boolean ifClassIn(Class<?> origClass, Class<?>... classes) {
        if (origClass == null) {
            return false;
        }

        for (Class<?> clazz : classes) {
            if (origClass.equals(clazz)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param origClass
     * @param annos
     * @return
     * @Description:判断class是否在指定class列表中
     * @author 千古龙少
     * @date 2019年9月22日 下午6:08:01
     */
    public static boolean ifAnnoIn(Class<?> origClass, Annotation... annos) {
        if (origClass == null) {
            return false;
        }

        for (Annotation anno : annos) {
            if (anno.getClass().isAssignableFrom(origClass)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param origClass
     * @param annos
     * @return
     * @Description:获取带有指定注解的注解
     * @author 千古龙少
     * @date 2019年10月6日 下午6:43:10
     */
    public static Annotation getAnnoIn(Class<?> origClass, Annotation... annos) {
        if (origClass == null || annos == null) {
            return null;
        }

        for (Annotation anno : annos) {
            if (anno.annotationType().isAssignableFrom(origClass)) {
                return anno;
            }
        }

        return null;
    }

    /**
     * @param origClass
     * @param classes
     * @return
     * @Description:校验class是否为指定class类型的父子类
     * @author 千古龙少
     * @date 2019年10月6日 下午5:08:37
     */
    public static boolean ifClassExtendsIn(Class<?> origClass, Class<?>... classes) {
        if (origClass == null) {
            return false;
        }

        for (Class<?> clazz : classes) {
            if (clazz.isAssignableFrom(origClass)) {
                return true;
            }
        }

        return false;
    }

}
