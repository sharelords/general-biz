package com.sharelords.biz.util;

import com.sharelords.biz.util.InvokeUtil.Child;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * @author 千古龙少
 * @Description: 模型类动态代理
 * @date 2019年3月19日 下午2:13:26
 */
public class ObjectClassInvoke {

    /**
     * 获取类的全局变量中，带有指定注解的属性名，获取第一个并返回
     *
     * @param clazz  类class
     * @param anoCls 注解class
     * @return String
     * @author 千古龙少
     * @date 2019年8月12日 下午3:43:45
     */
    public static String getFieldNameOfAno(Class<?> clazz, final Class<? extends Annotation> anoCls) {
        return InvokeUtil.searchFromList(Arrays.asList(clazz.getDeclaredFields()), new Child<String, Field>() {
            @Override
            public String group(Field field) {
                if (field.isAnnotationPresent(anoCls)) {
                    return field.getName();
                }

                return null;
            }
        });
    }

}
