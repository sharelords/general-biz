package com.sharelords.biz.util;

import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @author 千古龙少
 * @Description: 校验工具
 * @date 2019年5月24日 下午2:19:01
 */
public class CheckUtil {

    /**
     * 数组中存在元素为空值（null，空字符串，空集合，不支持数组）
     *
     * @param objs 参数数组
     * @return boolean
     * @author 千古龙少
     * @date 2019年9月17日 下午7:04:13
     */
    public static boolean existBlank(Object... objs) {
        if (objs == null || objs.length == 0) {
            return true;
        }

        for (Object obj : objs) {
            if (obj == null) {
                return true;
            }

            if (obj instanceof Number) {
                continue;
            }

            if (obj instanceof String) {
                if (StringUtils.isBlank((String) obj)) {
                    return true;
                }
                continue;
            }

            if (obj instanceof Collection) {
                if (((Collection<?>) obj).isEmpty()) {
                    return true;
                }
                continue;
            }

            if (obj instanceof Map<?, ?>) {
                if (((Map<?, ?>) obj).isEmpty()) {
                    return true;
                }
                continue;
            }

            if (obj.getClass().isArray()) {
                throw new RuntimeException("不支持数组类型！element=" + obj.getClass());
            }

        }

        return false;
    }

}
