package com.sharelords.biz.enums;

import com.sharelords.biz.annotation.join.Or;

import java.lang.annotation.Annotation;

/**
 * @author 千古龙少
 * @Description: 连接相关枚举
 * @date 2019年10月20日 下午5:19:04
 */
public enum JoinEnum {

    /**
     * 或
     */
    _Or(Or.class, "or"),
    ;

    /**
     * 注解
     */
    private Class<? extends Annotation> annoClass;
    /**
     * 值
     */
    private String value;

    JoinEnum(Class<? extends Annotation> annoClass, String value) {
        this.annoClass = annoClass;
        this.value = value;
    }

    public Class<? extends Annotation> getAnnoClass() {
        return annoClass;
    }

    public String getValue() {
        return value;
    }

    /**
     * @param annoClass
     * @return
     * @Description: 校验是否连接相关注解
     * @author 千古龙少
     * @date 2019年10月20日 下午6:06:59
     */
    public static boolean checkIfJoinAnno(Class<? extends Annotation> annoClass) {
        if (annoClass != null) {
            for (JoinEnum conditionValueEnum : values()) {
                if (annoClass.isAssignableFrom(conditionValueEnum.getAnnoClass())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param annoClass
     * @return
     * @Description: 获取连接(默认等于 " and ")
     * @author 千古龙少
     * @date 2019年10月20日 下午5:39:10
     */
    public static String getJoin(Class<? extends Annotation> annoClass) {
        if (annoClass != null) {
            for (JoinEnum joinEnum : values()) {
                if (annoClass.isAssignableFrom(joinEnum.getAnnoClass())) {
                    return joinEnum.getValue();
                }
            }
        }

        return "and";
    }

}
