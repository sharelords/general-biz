package com.sharelords.biz.enums;

import com.sharelords.biz.annotation.extra.method.NotEmpty;
import com.sharelords.biz.annotation.extra.method.NotNull;
import com.sharelords.biz.util.InvokeUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Description: 作用于方法上的复杂参数条件相关枚举
 * @Author: 千古龙少
 * @Time: 2019/11/30 23:30
 */
public enum ConditionExtraMethodValueEnum {

    /**
     * 非null
     */
    _NotNull(NotNull.class, "criteria.andIsNotNull(\"{0}\");"),
    /**
     * 非空
     */
    _NotEmpty(NotEmpty.class, "criteria.andIsNotNull(\"{0}\").andNotEqualTo(\"{0}\", \"\");"),
    ;

    /**
     * 注解
     */
    private Class<? extends Annotation> anoClass;
    /**
     * 值
     */
    private String value;

    ConditionExtraMethodValueEnum(Class<? extends Annotation> anoClass, String value) {
        this.anoClass = anoClass;
        this.value = value;
    }

    public Class<? extends Annotation> getAnoClass() {
        return anoClass;
    }

    public String getValue() {
        return value;
    }

    /**
     * 获取作用于方法上的条件
     *
     * @param method
     * @return java.util.List<java.lang.String>
     * @Author: 千古龙少
     * @Time: 2019/11/30 23:34
     */
    public static List<String> getMethodGlobalConditions(Method method) {
        if (method == null) {
            return Collections.emptyList();
        }

        List<String> conditionStrList = new ArrayList<>();

        // 非null
        NotNull notNullAno = method.getAnnotation(NotNull.class);
        if (notNullAno != null) {
            List<String> notNullConditionList = getConditionList(notNullAno.value(), _NotNull);
            if (notNullConditionList != null && !notNullConditionList.isEmpty()) {
                conditionStrList.addAll(notNullConditionList);
            }
        }

        // 非空
        NotEmpty notEmptyAno = method.getAnnotation(NotEmpty.class);
        if (notNullAno != null) {
            List<String> notEmptyConditionList = getConditionList(notEmptyAno.value(), _NotEmpty);
            if (notEmptyConditionList != null && !notEmptyConditionList.isEmpty()) {
                conditionStrList.addAll(notEmptyConditionList);
            }
        }

        return conditionStrList;
    }

    /**
     * 查询条件字符串组装
     *
     * @param propNameArr
     * @param valueEnum
     * @return java.util.List<java.lang.String>
     * @Author: 千古龙少
     * @Time: 2019/12/1 0:10
     */
    private static List<String> getConditionList(String[] propNameArr, final ConditionExtraMethodValueEnum valueEnum) {
        if (propNameArr == null || propNameArr.length == 0) {
            return Collections.emptyList();
        }

        List<String> propNameList = Arrays.asList(propNameArr);

        // 数据组装
        return InvokeUtil.transList(propNameList, new InvokeUtil.Child<String, String>() {
            @Override
            public String group(String propName) {
                return MessageFormat.format(valueEnum.getValue(), propName);
            }
        });
    }

}
