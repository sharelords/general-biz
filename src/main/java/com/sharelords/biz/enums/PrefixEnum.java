package com.sharelords.biz.enums;

import org.apache.commons.lang.StringUtils;

/**
 * @author 千古龙少
 * @Description: 前缀
 * @date 2019年10月6日 下午4:00:00
 */
public enum PrefixEnum {
    /**
     * 查询多条
     */
    queryBy("queryBy"),
    /**
     * 查询一条
     */
    queryOneBy("queryOneBy"),
    /**
     * 分页查询
     */
    queryPageBy("queryPageBy"),
    /**
     * 数量查询
     */
    queryCountBy("queryCountBy");

    /**
     * 前缀
     */
    private String prefix;

    PrefixEnum(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    /**
     * 校验前缀
     *
     * @param str
     * @return
     * @author 千古龙少
     * @date 2019年10月6日 下午4:10:16
     */
    public static boolean checkPrefix(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }

        for (PrefixEnum prefixEnum : values()) {
            if (str.startsWith(prefixEnum.getPrefix())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取所有前缀，逗号分隔
     *
     * @return
     * @author 千古龙少
     * @date 2019年10月6日 下午4:14:52
     */
    public static String getAllPrefixStr() {
        StringBuilder builder = new StringBuilder();
        for (PrefixEnum prefixEnum : values()) {
            if (builder.length() != 0) {
                builder.append(",");
            }

            builder.append(prefixEnum.getPrefix());
        }

        return builder.toString();
    }

}
