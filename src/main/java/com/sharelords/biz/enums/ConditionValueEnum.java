package com.sharelords.biz.enums;

import com.sharelords.biz.annotation.value.*;

import java.lang.annotation.Annotation;

/**
 * @author 千古龙少
 * @Description: 参数条件相关枚举
 * @date 2019年10月20日 下午5:19:04
 */
public enum ConditionValueEnum {

    /**
     * 大于
     */
    _Gt(Gt.class, "GreaterThan"),
    /**
     * 大于等于
     */
    _GtEqual(GtEqual.class, "GreaterThanOrEqualTo"),
    /**
     * 小于
     */
    _Lt(Lt.class, "LessThan"),
    /**
     * 小于等于
     */
    _LtEqual(LtEqual.class, "LessThanOrEqualTo"),
    /**
     * 不等于
     */
    _NotEqual(NotEqual.class, "NotEqualTo"),
    /**
     * in
     */
    _In(In.class, "In"),
    /**
     * like
     */
    _Like(Like.class, "Like"),
    /**
     * not like
     */
    _NotLike(NotLike.class, "NotLike"),
    ;

    /**
     * 注解
     */
    private Class<? extends Annotation> annoClass;
    /**
     * 值
     */
    private String value;

    private ConditionValueEnum(Class<? extends Annotation> annoClass, String value) {
        this.annoClass = annoClass;
        this.value = value;
    }

    public Class<? extends Annotation> getAnnoClass() {
        return annoClass;
    }

    public String getValue() {
        return value;
    }

    /**
     * @param annoClass
     * @return
     * @Description: 校验是否值相关注解
     * @author 千古龙少
     * @date 2019年10月20日 下午6:06:59
     */
    public static boolean checkIfValueAnno(Class<? extends Annotation> annoClass) {
        if (annoClass != null) {
            for (ConditionValueEnum conditionValueEnum : values()) {
                if (annoClass.isAssignableFrom(conditionValueEnum.getAnnoClass())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param annoClass
     * @return
     * @Description: 获取数值连接(默认等于 " EqualTo ")
     * @author 千古龙少
     * @date 2019年10月20日 下午5:39:10
     */
    public static String getValueJoin(Class<? extends Annotation> annoClass) {
        if (annoClass != null) {
            for (ConditionValueEnum conditionValueEnum : values()) {
                if (annoClass.isAssignableFrom(conditionValueEnum.getAnnoClass())) {
                    return conditionValueEnum.getValue();
                }
            }
        }

        return "EqualTo";
    }

}
