package com.sharelords.biz.enums;

import com.sharelords.biz.annotation.extra.EmptyOr;
import com.sharelords.biz.annotation.extra.NotEmptyAnd;
import com.sharelords.biz.annotation.extra.NotNullAnd;
import com.sharelords.biz.annotation.extra.NullOr;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;

/**
 * @Description: 复杂参数条件相关枚举
 * @Author: 千古龙少
 * @Time: 2019/11/30 23:30
 */
public enum ConditionExtraValueEnum {

    /**
     * null或
     */
    _NullOr(NullOr.class, "criteria_{0}.andIsNull(\"{1}\").or{2}(\"{1}\", {0});"),
    /**
     * 非null并且
     */
    _NotNullAnd(NotNullAnd.class, "criteria_{0}.andIsNotNull(\"{1}\").and{2}(\"{1}\", {0});"),
    /**
     * 空或
     */
    _EmptyOr(EmptyOr.class, "criteria_{0}.andIsNull(\"{1}\").orEqualTo(\"{1}\", \"\").or{2}(\"{1}\", {0});"),
    /**
     * 非空并且
     */
    _NotEmptyAnd(NotEmptyAnd.class, "criteria_{0}.andIsNotNull(\"{1}\").andNotEqualTo(\"{1}\", \"\").and{2}(\"{1}\", {0});"),
    ;

    /**
     * 换行
     */
    private static final String LINE_FEED = "\r\n";

    private static final String newCriteriaStr = "Criteria criteria_{0} = example.createCriteria();" + LINE_FEED
            + "{1}" + LINE_FEED
            + "example.{2}(criteria_{0});";

    /**
     * 注解
     */
    private Class<? extends Annotation> annoClass;
    /**
     * 值
     */
    private String value;

    ConditionExtraValueEnum(Class<? extends Annotation> annoClass, String value) {
        this.annoClass = annoClass;
        this.value = value;
    }

    public Class<? extends Annotation> getAnnoClass() {
        return annoClass;
    }

    public String getValue() {
        return value;
    }

    /**
     * 校验是否值相关注解
     *
     * @param annoClass
     * @return boolean
     * @Author: 千古龙少
     * @Time: 2019/11/30 17:32
     */
    public static boolean checkIfExtraValueAnno(Class<? extends Annotation> annoClass) {
        if (annoClass != null) {
            for (ConditionExtraValueEnum conditionExtraValueEnum : values()) {
                if (annoClass.isAssignableFrom(conditionExtraValueEnum.getAnnoClass())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 获取新的Criteria，并放入Example对象，字符串输出
     *
     * @param extraValueAnoClass 复杂条件注解
     * @param propName           属性名
     * @param paramName          参数名
     * @param valueJoinStr       条件数值字符串
     * @param joinStr            条件连接字符串
     * @return java.lang.String
     * @Author: 千古龙少
     * @Time: 2019/11/30 22:37
     */
    public static String getNewCriteriaStr(Class<? extends Annotation> extraValueAnoClass, String propName, String paramName, String valueJoinStr, String joinStr) {
        ConditionExtraValueEnum extraValueEnum = getEnumByAnoClass(extraValueAnoClass);
        if (extraValueEnum == null) {
            return "";
        }

        // 条件字符串
        String conditionStr = MessageFormat.format(extraValueEnum.getValue(), paramName, propName, valueJoinStr);

        // 结果字符串
        return MessageFormat.format(newCriteriaStr, paramName, conditionStr, joinStr);
    }

    /**
     * 获取枚举项
     *
     * @param extraValueAnoClass
     * @return com.sharelords.biz.enums.ConditionExtraValueEnum
     * @Author: 千古龙少
     * @Time: 2019/11/30 22:14
     */
    private static ConditionExtraValueEnum getEnumByAnoClass(Class<? extends Annotation> extraValueAnoClass) {
        if (extraValueAnoClass == null) {
            return null;
        }

        for (ConditionExtraValueEnum conditionExtraValueEnum : values()) {
            if (extraValueAnoClass.isAssignableFrom(conditionExtraValueEnum.getAnnoClass())) {
                return conditionExtraValueEnum;
            }
        }

        return null;
    }


}
