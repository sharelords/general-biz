package com.sharelords.biz.enums;

/**
 * @author 千古龙少
 * @Description: 查询名称枚举项（与BizQueryUtil方法名对应）
 * @date 2019年11月2日 下午2:26:04
 */
public enum QueryNameTypeEnum {

    /**
     * 查询一条
     */
    queryLimitOne("queryLimitOne", "queryLimitOne"),
    /**
     * 查询多条
     */
    queryMore("queryMore", "queryMoreDefAll"),
    /**
     * 分页查询
     */
    queryPage("queryPage", "queryPageDefAll"),
    /**
     * 查询数量
     */
    queryCount("queryCount", "queryCountDefAll"),
    ;

    /**
     * 查询名称
     */
    private String name;
    private String nameButAll;

    QueryNameTypeEnum(String name, String nameButAll) {
        this.name = name;
        this.nameButAll = nameButAll;
    }

    public String getName() {
        return name;
    }

    public String getNameButAll() {
        return nameButAll;
    }
}
