package com.sharelords.biz.register;

import com.sharelords.biz.creator.memory.MemoryClassLoader;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.Map;

/**
 * @Description: 内存class注册
 * @Author: 千古龙少
 * @Time: 2019/11/23 18:03
 */
public class MemoryClassBeanRegister {

    private BeanDefinitionRegistry registry;

    public MemoryClassBeanRegister(BeanDefinitionRegistry registry) {
        this.registry = registry;
    }

    /**
     * 向spring容器注册
     */
    public void regist() {
        Map<String, Class<?>> interfaceAndImplClazzMap = null;
        try {
            interfaceAndImplClazzMap = MemoryClassLoader.getInstance().getPreparedBizImplClasses();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        for (Map.Entry<String, Class<?>> entry : interfaceAndImplClazzMap.entrySet()) {
            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(entry.getValue());
            AbstractBeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
            registry.registerBeanDefinition(entry.getKey(), beanDefinition);
        }
    }

}
