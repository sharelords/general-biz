package com.sharelords.biz;

import com.sharelords.biz.common.BaseBiz;
import tk.mybatis.mapper.common.Mapper;

/**
 * @param <Bo>     业务模型
 * @param <Entity> 数据模型
 * @param <D>      主键类型
 * @param <Dao>    数据层接口
 * @author 千古龙少
 * @Description: 使用本功能的biz层接口必须继承本接口
 * @date 2019年9月8日 下午10:25:03
 */
public interface Biz<Bo, Entity, D, Dao extends Mapper<Entity>> extends BaseBiz<Bo, D> {

}
