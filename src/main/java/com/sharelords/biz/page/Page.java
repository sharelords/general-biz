package com.sharelords.biz.page;

import java.io.Serializable;

/**
 * @author 千古龙少
 * @Description:分页
 * @date 2019年11月3日 下午1:55:42
 */
public class Page implements Serializable {

    private static final long serialVersionUID = 2853433308636507108L;

    /**
     * 页码
     */
    private int pageNum;
    /**
     * 每页展示的数量
     */
    private int pageSize;
    /**
     * 总页数
     */
    private int totalPage;
    /**
     * 总条数
     */
    private long totalNum;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(long totalNum) {
        this.totalNum = totalNum;
    }

}
