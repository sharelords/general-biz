package com.sharelords.biz.config;

import java.io.Serializable;

/**
 * @author 千古龙少
 * @Description: general-biz相关属性设置
 * @date 2020年6月18日 上午1:52:33
 */
public class GeneralBizConfig implements Serializable {

    private static final long serialVersionUID = 7141659752920131786L;

    private static GeneralBizConfig config = new GeneralBizConfig();

    private GeneralBizConfig() {
    }

    public static GeneralBizConfig getInstance() {
        return config;
    }

    /**
     * biz文件扫描路径
     */
    public String basePackages;

    /**
     * 是否保留java资源文件（默认不保留）
     */
    public boolean remainJavaSrcFile = false;

    /**
     * java资源文件保存路径，默认项目上一层目录下新建的bizTempJavaFile文件夹内
     */
    public String srcFilePath;

}
