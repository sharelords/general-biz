package com.sharelords.biz.common;

import java.util.List;

/**
 * @author 千古龙少
 * @Description: 公用biz组件
 * @date 2019年1月13日 下午5:03:31
 */
public interface BaseBiz<Bo, D> {

    /**
     * 数据保存（保存null），并返回成功条数
     *
     * @param bo
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:33
     */
    Integer save(Bo bo);

    /**
     * 数据保存（不保存null），并返回成功条数
     *
     * @param bo
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:33
     */
    Integer saveSelective(Bo bo);

    /**
     * 数据更新（保存null）
     *
     * @param bo
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:33
     */
    Integer update(Bo bo);

    /**
     * 数据更新（不保存null）
     *
     * @param bo
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:33
     */
    Integer updateSelective(Bo bo);

    /**
     * 物理删除记录
     *
     * @param d
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:33
     */
    Integer deletePhysically(D d);

    /**
     * 根据主键查询详情
     *
     * @param d
     * @return Bo
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:34
     */
    Bo queryByPrimaryKey(D d);

    /**
     * 根据数据模型对象查询列表
     *
     * @param bo
     * @return java.util.List<Bo>
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:34
     */
    List<Bo> queryByBo(Bo bo);

    /**
     * 查询所有记录
     *
     * @param
     * @return java.util.List<Bo>
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:34
     */
    List<Bo> queryAll();

    /**
     * 根据主键批量查询记录列表
     *
     * @param idList
     * @return java.util.List<Bo>
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:34
     */
    List<Bo> queryReCordsByIdList(List<D> idList);

    /**
     * 根据数据模型对象查询数量
     *
     * @param bo
     * @return java.lang.Integer
     * @Author: 千古龙少
     * @Time: 2019/12/10 23:34
     */
    Integer queryCountByBo(Bo bo);

}
